package com.inriver.productadapter.inbound.events;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
public class InriverMediaDownloadHookTest {

    private InriverMediaDownloadHook inriverMediaDownloadHook;

    private MediaService mediaService;

    @Before
    public void setup() {
        mediaService = mock(MediaService.class);
        inriverMediaDownloadHook = new InriverMediaDownloadHook(mediaService);
    }

    @Test
    public void downloadImageFromWrongExternalUrl() {
        MediaModel mediaModel = mock(MediaModel.class);

        when(mediaModel.getExternalURL()).thenReturn("https://cdn.pixabay.com/photo/2016/11/29/05/45/astronomy_1280.jpg");

        inriverMediaDownloadHook.execute(mediaModel);
        verifyZeroInteractions(mediaService);
    }

    @Test
    public void downloadImageFromMalformedExternalUrl() {
        MediaModel mediaModel = mock(MediaModel.class);

        when(mediaModel.getExternalURL()).thenReturn("hts://cdn.piay.om/pto/205/45/astronomy_1280.jpg");

        inriverMediaDownloadHook.execute(mediaModel);
        verifyZeroInteractions(mediaService);
    }

    @Test
    public void downloadImageFromExternalUrl() {
        MediaModel mediaModel = mock(MediaModel.class);

        when(mediaModel.getExternalURL()).thenReturn("https://cdn.pixabay.com/photo/2016/11/29/05/45/astronomy-1867616_1280.jpg");

        inriverMediaDownloadHook.execute(mediaModel);
        verify(mediaService).setStreamForMedia(any(), any());
    }

}
