package com.inriver.productadapter.inbound.services.impl;

import com.inriver.productadapter.inbound.services.InriverProductFeaturePersistenceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.*;
import de.hybris.platform.classification.ClassificationClassesResolverStrategy;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

@UnitTest
public class DefaultInriverProductFeaturePersistenceServiceTest {

    private InriverProductFeaturePersistenceService sut;

    private ModelService modelService;
    private ClassificationClassesResolverStrategy classResolverStrategy;
    private ClassificationSystemService classificationSystemService;

    @Before
    public void setup() {
        modelService = mock(ModelService.class);
        classResolverStrategy = mock(ClassificationClassesResolverStrategy.class);
        classificationSystemService = mock(ClassificationSystemService.class);
        sut = new DefaultInriverProductFeaturePersistenceService(classResolverStrategy, modelService, classificationSystemService);
    }

    @Test
    public void persistSingleStringValue() {
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemModel classificationSystemModel = mock(ClassificationSystemModel.class);
        ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        ProductModel productModel = mock(ProductModel.class);
        ProductFeatureModel productFeatureModel = mock(ProductFeatureModel.class);

        when(classificationSystemModel.getId()).thenReturn("Electronics");
        when(classificationSystemVersionModel.getCatalog()).thenReturn(classificationSystemModel);
        when(classificationAttributeModel.getCode()).thenReturn("description");
        when(classificationClassModel.getCode()).thenReturn("lens");
        when(classAttributeAssignmentModel.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.STRING);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classAttributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(classAttributeAssignmentModel.getPk()).thenReturn(PK.fromLong(10));
        when(classAttributeAssignmentModel.getSystemVersion()).thenReturn(classificationSystemVersionModel);

        when(productFeatureModel.getQualifier()).thenReturn("Electronics/Photography/lens/description");
        when(productFeatureModel.getValue()).thenReturn("my lens description");

        when(productModel.getFeatures()).thenReturn(new ArrayList<>(List.of(productFeatureModel)));

        when(classResolverStrategy.resolve(productModel)).thenReturn(Set.of(classificationClassModel));
        when(classResolverStrategy.getAllClassAttributeAssignments(Set.of(classificationClassModel))).thenReturn(List.of(classAttributeAssignmentModel));

        sut.persist(productModel);

        verify(productFeatureModel).setClassificationAttributeAssignment(classAttributeAssignmentModel);
        verify(productFeatureModel).setProduct(productModel);
        verify(productFeatureModel).setQualifier("Electronics/Photography/lens.description");
        verify(productFeatureModel).setValue("my lens description");
        verify(productFeatureModel).setValuePosition(null);
    }

    @Test
    public void persistSingleNumberValue() {
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemModel classificationSystemModel = mock(ClassificationSystemModel.class);
        ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        ProductModel productModel = mock(ProductModel.class);
        ProductFeatureModel productFeatureModel = mock(ProductFeatureModel.class);

        when(classificationSystemModel.getId()).thenReturn("Electronics");
        when(classificationSystemVersionModel.getCatalog()).thenReturn(classificationSystemModel);
        when(classificationAttributeModel.getCode()).thenReturn("size");
        when(classificationClassModel.getCode()).thenReturn("lens");
        when(classAttributeAssignmentModel.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.NUMBER);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classAttributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(classAttributeAssignmentModel.getPk()).thenReturn(PK.fromLong(10));
        when(classAttributeAssignmentModel.getSystemVersion()).thenReturn(classificationSystemVersionModel);

        when(productFeatureModel.getQualifier()).thenReturn("Electronics/Photography/lens/size");
        when(productFeatureModel.getValue()).thenReturn("123.45");

        when(productModel.getFeatures()).thenReturn(new ArrayList<>(List.of(productFeatureModel)));

        when(classResolverStrategy.resolve(productModel)).thenReturn(Set.of(classificationClassModel));
        when(classResolverStrategy.getAllClassAttributeAssignments(Set.of(classificationClassModel))).thenReturn(List.of(classAttributeAssignmentModel));

        sut.persist(productModel);

        verify(productFeatureModel).setClassificationAttributeAssignment(classAttributeAssignmentModel);
        verify(productFeatureModel).setProduct(productModel);
        verify(productFeatureModel).setQualifier("Electronics/Photography/lens.size");
        verify(productFeatureModel).setValue(123.45);
        verify(productFeatureModel).setValuePosition(null);
    }

    @Test
    public void persistMultiStringValue() {
        ClassificationSystemVersionModel classificationSystemVersionModel = mock(ClassificationSystemVersionModel.class);
        ClassificationSystemModel classificationSystemModel = mock(ClassificationSystemModel.class);
        ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        ClassAttributeAssignmentModel classAttributeAssignmentModel = mock(ClassAttributeAssignmentModel.class);
        ClassificationAttributeModel classificationAttributeModel = mock(ClassificationAttributeModel.class);
        ProductModel productModel = mock(ProductModel.class);
        ProductFeatureModel productFeatureModel1 = mock(ProductFeatureModel.class);
        ProductFeatureModel productFeatureModel2 = mock(ProductFeatureModel.class);

        when(classificationSystemModel.getId()).thenReturn("Electronics");
        when(classificationSystemVersionModel.getCatalog()).thenReturn(classificationSystemModel);
        when(classificationAttributeModel.getCode()).thenReturn("descriptions");
        when(classificationClassModel.getCode()).thenReturn("lens");
        when(classAttributeAssignmentModel.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.STRING);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classAttributeAssignmentModel.getClassificationClass()).thenReturn(classificationClassModel);
        when(classAttributeAssignmentModel.getMultiValued()).thenReturn(true);
        when(classAttributeAssignmentModel.getPk()).thenReturn(PK.fromLong(10));
        when(classAttributeAssignmentModel.getSystemVersion()).thenReturn(classificationSystemVersionModel);

        when(productFeatureModel1.getQualifier()).thenReturn("Electronics/Photography/lens/descriptions");
        when(productFeatureModel1.getValue()).thenReturn("my lens description 1");

        when(productFeatureModel2.getQualifier()).thenReturn("Electronics/Photography/lens/descriptions");
        when(productFeatureModel2.getValue()).thenReturn("my lens description 2");

        when(productModel.getFeatures()).thenReturn(new ArrayList<>(List.of(productFeatureModel1, productFeatureModel2)));

        when(classResolverStrategy.resolve(productModel)).thenReturn(Set.of(classificationClassModel));
        when(classResolverStrategy.getAllClassAttributeAssignments(Set.of(classificationClassModel))).thenReturn(List.of(classAttributeAssignmentModel));

        sut.persist(productModel);

        verify(productFeatureModel1).setClassificationAttributeAssignment(classAttributeAssignmentModel);
        verify(productFeatureModel1).setProduct(productModel);
        verify(productFeatureModel1).setQualifier("Electronics/Photography/lens.descriptions");
        verify(productFeatureModel1).setValue("my lens description 1");
        verify(productFeatureModel1).setValuePosition(1);

        verify(productFeatureModel2).setClassificationAttributeAssignment(classAttributeAssignmentModel);
        verify(productFeatureModel2).setProduct(productModel);
        verify(productFeatureModel2).setQualifier("Electronics/Photography/lens.descriptions");
        verify(productFeatureModel2).setValue("my lens description 2");
        verify(productFeatureModel2).setValuePosition(2);
    }

}