package com.inriver.productadapter.inbound.events;

import com.inriver.productadapter.inbound.services.InriverProductFeaturePersistenceService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
public class InriverProductFeaturePersistenceHookTest {

    private InriverProductFeaturePersistenceHook sut;

    private ModelService modelService;
    private InriverProductFeaturePersistenceService productFeaturePersistenceService;

    @Before
    public void setup() {
        modelService = mock(ModelService.class);
        productFeaturePersistenceService = mock(InriverProductFeaturePersistenceService.class);

        sut = new InriverProductFeaturePersistenceHook(modelService, productFeaturePersistenceService);
    }

    @Test
    public void execute() {
        ProductModel productModel = mock(ProductModel.class);
        ProductFeatureModel currentProductFeatureModel = mock(ProductFeatureModel.class);
        ProductFeatureModel newProductFeatureModel = mock(ProductFeatureModel.class);

        when(productModel.getFeatures()).thenReturn(List.of(currentProductFeatureModel, newProductFeatureModel));

        when(modelService.isNew(currentProductFeatureModel)).thenReturn(false);
        when(modelService.isNew(newProductFeatureModel)).thenReturn(true);

        Optional<ItemModel> actual = sut.execute(productModel);
        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get()).isEqualTo(productModel);

        verify(productModel).setFeatures(List.of(newProductFeatureModel));
        verify(productFeaturePersistenceService).persist(productModel);
        verify(modelService).removeAll(List.of(currentProductFeatureModel));
    }

    @Test
    public void executeWithoutCurrentFeatures() {
        ProductModel productModel = mock(ProductModel.class);
        ProductFeatureModel newProductFeatureModel = mock(ProductFeatureModel.class);

        when(productModel.getFeatures()).thenReturn(List.of(newProductFeatureModel));

        when(modelService.isNew(newProductFeatureModel)).thenReturn(true);

        Optional<ItemModel> actual = sut.execute(productModel);
        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get()).isEqualTo(productModel);

        verify(productModel).setFeatures(List.of(newProductFeatureModel));
        verify(productFeaturePersistenceService).persist(productModel);
        verify(modelService, never()).removeAll(anyCollection());
    }

}