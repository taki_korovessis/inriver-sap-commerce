package com.inriver.productadapter.inbound.events;

import com.inriver.productadapter.inbound.services.InriverProductFeaturePersistenceService;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.odata2services.odata.persistence.hook.PrePersistHook;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class InriverProductFeaturePersistenceHook implements PrePersistHook {

    private static final Logger LOG = LoggerFactory.getLogger(InriverProductFeaturePersistenceHook.class);

    private final ModelService modelService;
    private final InriverProductFeaturePersistenceService productFeaturePersistenceService;

    public InriverProductFeaturePersistenceHook(ModelService modelService, InriverProductFeaturePersistenceService productFeaturePersistenceService) {
        this.modelService = modelService;
        this.productFeaturePersistenceService = productFeaturePersistenceService;
    }

    @Override
    public Optional<ItemModel> execute(ItemModel item) {
        if (item instanceof ProductModel) {
            LOG.info("The persistence hook inriverProductFeaturePersistenceHook is called!");
            final ProductModel product = (ProductModel) item;

            final List<ProductFeatureModel> toRemove = emptyIfNull(product.getFeatures()).stream()
                    .filter(f -> !modelService.isNew(f))
                    .collect(Collectors.toList());

            final List<ProductFeatureModel> toPersist = emptyIfNull(product.getFeatures()).stream()
                    .filter(modelService::isNew)
                    .collect(Collectors.toList());
            product.setFeatures(toPersist);
            productFeaturePersistenceService.persist(product);

            if (isNotEmpty(toRemove)) {
                modelService.removeAll(toRemove);
            }
        }
        return Optional.of(item);
    }

}
