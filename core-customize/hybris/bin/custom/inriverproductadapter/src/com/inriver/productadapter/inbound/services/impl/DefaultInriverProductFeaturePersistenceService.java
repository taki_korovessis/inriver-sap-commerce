package com.inriver.productadapter.inbound.services.impl;

import com.inriver.productadapter.inbound.services.InriverProductFeaturePersistenceService;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.ClassificationClassesResolverStrategy;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static de.hybris.platform.impex.constants.ImpExConstants.Syntax.DEFAULT_COLLECTION_VALUE_DELIMITER;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.collections4.CollectionUtils.*;

public class DefaultInriverProductFeaturePersistenceService implements InriverProductFeaturePersistenceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultInriverProductFeaturePersistenceService.class);

    private static final String DEFAULT_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    private static final int QUALIFIER_SIZE = 4;
    private static final int QUALIFIER_CLASSIFICATION_SYSTEM = 0;
    private static final int QUALIFIER_CLASSIFICATION_SYSTEM_VERSION = 1;
    private static final int QUALIFIER_CLASSIFICATION_CLASS = 2;
    private static final int QUALIFIER_FEATURE = 3;
    private static final String QUALIFIER_SPLITTER = "/";

    private static final String IMPEX_NONEXISTEND_CLSATTRVALUE_FALLBACK_KEY = "impex.nonexistend.clsattrvalue.fallback.enabled";

    private final ClassificationClassesResolverStrategy classResolverStrategy;
    private final ModelService modelService;
    private final ClassificationSystemService classificationSystemService;

    private final String collectionDelimiter;

    public DefaultInriverProductFeaturePersistenceService(ClassificationClassesResolverStrategy classResolverStrategy,
                                                          ModelService modelService,
                                                          ClassificationSystemService classificationSystemService) {
        this(classResolverStrategy, modelService, classificationSystemService, String.valueOf(DEFAULT_COLLECTION_VALUE_DELIMITER));
    }

    public DefaultInriverProductFeaturePersistenceService(ClassificationClassesResolverStrategy classResolverStrategy,
                                                          ModelService modelService,
                                                          ClassificationSystemService classificationSystemService,
                                                          String collectionDelimiter) {
        this.classResolverStrategy = classResolverStrategy;
        this.modelService = modelService;
        this.classificationSystemService = classificationSystemService;
        this.collectionDelimiter = collectionDelimiter;
    }

    @Override
    public void persist(ProductModel product) {
        final List<ProductFeatureModel> productFeatures = product.getFeatures();

        setProductFeatureProduct(productFeatures, product);

        final Set<ClassificationClassModel> classificationClasses = classResolverStrategy.resolve(product);

        if (!isValidProductFeatures(productFeatures) || classificationClasses.isEmpty()) {
            cleanProductFeatureProduct(productFeatures);
            return;
        }

        final Set<PK> singleAttributeValue = new HashSet<>();

        final Map<PK, Integer> multipleAttributeValuePosition = new HashMap<>();

        final List<ProductFeatureModel> extraProductFeatures = new ArrayList<>();

        for (final Iterator<ProductFeatureModel> i = productFeatures.iterator(); i.hasNext(); ) {
            final ProductFeatureModel productFeature = i.next();

            final String[] qualifierInfo = StringUtils.split(productFeature.getQualifier(), QUALIFIER_SPLITTER);

            final ClassAttributeAssignmentModel assignmentModel = retrieveClassAttributeAssignmentModel(classificationClasses, qualifierInfo);

            if (assignmentModel == null || !assignmentModel.getMultiValued() && !singleAttributeValue.add(assignmentModel.getPk())) {
                i.remove();
                continue;
            } else if (assignmentModel.getMultiValued()) {
                multipleAttributeValuePosition.put(assignmentModel.getPk(), multipleAttributeValuePosition.getOrDefault(assignmentModel.getPk(), 0) + 1);
            }

            final List<Object> values = translateAwareValue(assignmentModel, productFeature.getValue(), qualifierInfo[QUALIFIER_FEATURE]);
            final ClassificationClassModel classificationClass = assignmentModel.getClassificationClass();

            productFeature.setValue(values.get(0));
            productFeature.setUnit(assignmentModel.getUnit());
            productFeature.setClassificationAttributeAssignment(assignmentModel);
            productFeature.setQualifier(buildQualifier(classificationClass, qualifierInfo));
            productFeature.setValuePosition(multipleAttributeValuePosition.get(assignmentModel.getPk()));

            //if attribute is range type, which has two numeric values.
            final int RANGE_VALUES = 2;
            if (values.size() == RANGE_VALUES) {
                final ProductFeatureModel newProductFeature = cloneProductFeature(productFeature);
                newProductFeature.setValue(values.get(1));
                newProductFeature.setUnit(assignmentModel.getUnit());
                //if attribute is multiple value
                multipleAttributeValuePosition.put(assignmentModel.getPk(), multipleAttributeValuePosition.get(assignmentModel.getPk()) + 1); //if attribute is multiple value
                newProductFeature.setValuePosition(multipleAttributeValuePosition.get(assignmentModel.getPk()));
                extraProductFeatures.add(newProductFeature);
            }
        }

        productFeatures.addAll(extraProductFeatures);
    }

    private List<Object> translateAwareValue(final ClassAttributeAssignmentModel assignment, final Object value, final String qualifier) {
        final List<Object> values = new ArrayList<>();

        for (final String singleStr : splitFeatureValues(assignment, value, qualifier)) {
            if (Optional.ofNullable(singleStr).isPresent()) {
                Object transValue = null;
                try {
                    transValue = getSingleProductFeatureValue(assignment, singleStr, qualifier);
                } catch (final JaloInvalidParameterException e) {
                    if (Config.getBoolean(IMPEX_NONEXISTEND_CLSATTRVALUE_FALLBACK_KEY, false)) {
                        LOG.debug("Fallback ENABLED");
                        LOG.warn(String.format(
                                "Value %s is not of type %s will use type string as fallback (%s)", value, assignment.getAttributeType().getCode(), e.getMessage()), e);
                    } else {
                        LOG.debug("Fallback DISABLED. Marking line as unresolved. Will try to import value in another pass", e);
                    }
                }
                values.add(transValue);
            }
        }

        return values;
    }

    private void setProductFeatureProduct(final List<ProductFeatureModel> productFeatures, final ProductModel product) {
        if (productFeatures != null) {
            productFeatures.forEach(feature -> feature.setProduct(product));
        }
    }

    private void cleanProductFeatureProduct(final List<ProductFeatureModel> productFeatures) {
        if (productFeatures != null) {
            productFeatures.removeIf(t -> t.getPk() == null);
        }
    }

    private boolean isValidProductFeatures(final List<ProductFeatureModel> productFeatures) {
        return isNotEmpty(productFeatures) && productFeatures.stream().noneMatch(p -> p.getPk() != null);
    }

    private ClassAttributeAssignmentModel retrieveClassAttributeAssignmentModel(final Set<ClassificationClassModel> classificationClasses, final String[] qualifierInfo) {
        if (size(qualifierInfo) < QUALIFIER_SIZE) {
            return null;
        }
        return emptyIfNull(classResolverStrategy.getAllClassAttributeAssignments(classificationClasses)).stream()
                .filter(assignment -> assignment.getClassificationAttribute().getCode().equals(qualifierInfo[QUALIFIER_FEATURE]))
                .filter(assignment -> assignment.getClassificationClass().getCode().equals(qualifierInfo[QUALIFIER_CLASSIFICATION_CLASS]))
                .filter(assignment -> assignment.getSystemVersion().getCatalog().getId().equals(qualifierInfo[QUALIFIER_CLASSIFICATION_SYSTEM]))
                .findFirst()
                .orElse(null);
    }

    private Object getSingleProductFeatureValue(final ClassAttributeAssignmentModel assignment, final Object featureValue, final String qualifier) {
        Object returnValue = featureValue;
        if (featureValue instanceof String) {
            final ClassificationAttributeTypeEnum type = assignment.getAttributeType();
            switch (type) {
                case BOOLEAN:
                    returnValue = Boolean.valueOf(formatValue(featureValue, qualifier));
                    break;
                case ENUM:
                    returnValue = classificationSystemService.getAttributeValueForCode(assignment.getSystemVersion(), (String) featureValue);
                    validateParameterNotNull(returnValue, "No such attribute value: null");
                    break;
                case NUMBER:
                    returnValue = Double.valueOf(convertNumeric(featureValue, qualifier));
                    break;
                case STRING:
                    returnValue = formatValue(featureValue, qualifier);
                    break;
                case DATE:
                    final SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN);
                    try {
                        returnValue = df.parse(formatValue(featureValue, qualifier));
                    } catch (final ParseException e) {
                        throw new IllegalArgumentException("Invalid date: " + formatValue(featureValue, qualifier));
                    }
                    break;
                case REFERENCE:
                    break;
            }
        }
        return returnValue;
    }

    private String formatValue(final Object value, final String qualifier) {
        if (value == null) {
            return null;
        }
        return ((String) value).replace(qualifier.toUpperCase() + "_", "");
    }

    private String buildQualifier(final ClassificationClassModel superCategory, String[] qualifierInfo) {
        return new StringBuilder()
                .append(qualifierInfo[QUALIFIER_CLASSIFICATION_SYSTEM])
                .append(QUALIFIER_SPLITTER).append(qualifierInfo[QUALIFIER_CLASSIFICATION_SYSTEM_VERSION])
                .append(QUALIFIER_SPLITTER).append(superCategory.getCode())
                .append(".").append(qualifierInfo[QUALIFIER_FEATURE].toLowerCase())
                .toString();
    }

    private String convertNumeric(final Object value, final String qualifier) {
        final String numericValue = formatValue(value, qualifier);
        final BigDecimal number = new BigDecimal(numericValue);
        return number.stripTrailingZeros().toPlainString();
    }

    private ProductFeatureModel cloneProductFeature(final ProductFeatureModel productFeature) {
        return modelService.clone(productFeature);
    }

    private List<String> splitFeatureValues(final ClassAttributeAssignmentModel assignment, final Object valueCollection, final String qualifier) {
        if (valueCollection == null) {
            return Collections.emptyList();
        }

        final String[] values = ((String) valueCollection).split(Pattern.quote(collectionDelimiter));
        final List<String> returnValues = new ArrayList<>();
        final int RANGE_VALUES = 2;
        returnValues.add(values[0]);
        if (assignment.getRange() && values.length == RANGE_VALUES) {
            if (new BigDecimal(formatValue(values[0], qualifier)).compareTo(new BigDecimal(values[1])) > 0) {
                returnValues.add(convertMaxNumeric(assignment.getFormatDefinition()));
            } else {
                returnValues.add(values[1]);
            }
        }
        return returnValues;
    }

    private String convertMaxNumeric(String format) {
        return format == null ? "9999" : format.split(";")[0].replace(",", "").replace('#', '9').replace('0', '9');
    }

}
