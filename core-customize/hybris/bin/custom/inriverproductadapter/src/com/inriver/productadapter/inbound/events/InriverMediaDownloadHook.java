package com.inriver.productadapter.inbound.events;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.odata2services.odata.persistence.hook.PostPersistHook;
import de.hybris.platform.servicelayer.media.MediaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class InriverMediaDownloadHook implements PostPersistHook {
    private static final Logger LOG = LoggerFactory.getLogger(InriverMediaDownloadHook.class);

    private final MediaService mediaService;

    public InriverMediaDownloadHook(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    @Override
    public void execute(final ItemModel item) {
        if (item instanceof MediaModel) {
            LOG.info("The media download hook inriverMediaDownloadHook is called!");
            final MediaModel media = (MediaModel) item;
            try {
                final URL mediaURL = new URL(media.getExternalURL());
                try (final InputStream is = mediaURL.openStream()) {
                    mediaService.setStreamForMedia(media, is);
                }
            } catch (IOException e) {
                LOG.error(String.format("Error downloading media '%s' from %s", media.getCode(), media.getExternalURL()), e);
            }
        }
    }
}
