/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.inriver.productadapter.constants;

/**
 * Global class for all Inriverproductadapter constants. You can add global constants for your extension into this class.
 */
public final class InriverproductadapterConstants extends GeneratedInriverproductadapterConstants {
    public static final String EXTENSIONNAME = "inriverproductadapter";

    private InriverproductadapterConstants() {
        //empty to avoid instantiating this constant class
    }

}
