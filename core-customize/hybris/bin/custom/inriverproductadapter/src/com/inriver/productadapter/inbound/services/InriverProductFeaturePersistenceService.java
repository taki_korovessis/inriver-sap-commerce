package com.inriver.productadapter.inbound.services;

import de.hybris.platform.core.model.product.ProductModel;

public interface InriverProductFeaturePersistenceService {

    void persist(ProductModel product);
}
